FROM php:7.0-fpm-alpine

RUN set -xe && \
        apk add --update libmcrypt && \
        apk add --no-cache --virtual .backend-deps libmcrypt-dev && \
        docker-php-ext-install mysqli mcrypt && \
        apk del .backend-deps